#!/bin/sh

sudo apt-get update
sudo apt-get install -y python-software-properties
sudo add-apt-repository -y ppa:rquillo/ansible
sudo apt-get update
sudo apt-get install -y ansible git

cd /tmp
sudo git clone https://bitbucket.org/flowcom/ansibleinside

cd ansibleinside
ansible-playbook -i ansible/localhost ansible/site.yml

